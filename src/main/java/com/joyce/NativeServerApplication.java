package com.joyce;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
@SpringBootApplication
@EnableConfigServer
@EnableEurekaClient
@EnableDiscoveryClient
public class NativeServerApplication {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NativeServerApplication.class);

	public static void main(String[] args) { 
		SpringApplication.run(NativeServerApplication.class, args); 
	}

}

package com.joyce.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigServerController {

	@RequestMapping(value = "/hi")
	public String hi(){
		return "Hi this is Native Config Server !";
	}
}
